DROP TABLE IF EXISTS order_rows;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS authorities;
DROP TABLE IF EXISTS users;
DROP SEQUENCE IF EXISTS seq1;

CREATE SEQUENCE seq1 AS INTEGER START WITH 1;

CREATE TABLE orders (
  id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq1'),
  order_number VARCHAR(255)
);

CREATE TABLE order_rows (
  item_name VARCHAR(255),
  price NUMERIC(15,4),
  quantity INT,
  orders_id BIGINT,
  FOREIGN KEY (orders_id)
    REFERENCES orders ON DELETE CASCADE
);

CREATE TABLE USERS (
 username VARCHAR(255) NOT NULL PRIMARY KEY,
 password VARCHAR(255) NOT NULL,
 enabled BOOLEAN NOT NULL,
 first_name VARCHAR(255) NOT NULL
);

CREATE TABLE AUTHORITIES (
 username VARCHAR(50) NOT NULL,
 authority VARCHAR(50) NOT NULL,
 FOREIGN KEY (username) REFERENCES USERS
   ON DELETE CASCADE
);

INSERT INTO users (USERNAME, PASSWORD, ENABLED, FIRST_NAME)
 VALUES ('user', '$2y$10$N9budlyDg0UwLFRDusSpnOTLn0CgU5OrsOULIzsVuDRh6gbujy/mS', true, 'Juhan');
INSERT INTO users (USERNAME, PASSWORD, ENABLED, FIRST_NAME)
 VALUES ('admin', '$2y$10$Cg72pGILg4zW0dKSoCcNw.VpK2DyqtJ6X5fEuXfWVbCQZqzEmxeNq', true, 'Kalle');
INSERT INTO AUTHORITIES (USERNAME, AUTHORITY)
 VALUES ('user', 'ROLE_USER');
INSERT INTO AUTHORITIES (USERNAME, AUTHORITY)
 VALUES ('admin', 'ROLE_ADMIN');