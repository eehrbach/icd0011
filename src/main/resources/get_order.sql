SELECT O.id as order_id,
       O.order_number,
       OROW.item_name,
       OROW.quantity,
       OROW.price,
       OROW.order_id as order_row_order_id
FROM orders O
         LEFT JOIN order_rows OROW ON O.id = OROW.order_id
WHERE O.id = ?