package test.config;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:/application.properties")
public class DatasourceConfig {
    private static final String DB_INIT_SCRIPT = "init_db.sql";

    private final Environment env;

    @Autowired
    public DatasourceConfig(Environment env) {
        this.env = env;
    }

    @Bean
    public DataSource dataSource() {
        var dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("db.driver", "org.hsqldb.jdbc.JDBCDriver"));
        dataSource.setUrl(env.getProperty("db.url"));

        var dbPopulator = new ResourceDatabasePopulator();
        dbPopulator.addScript(new ClassPathResource(DB_INIT_SCRIPT));
        dbPopulator.execute(dataSource);

        return dataSource;
    }

    @Bean
    public EntityManagerFactory entityManagerFactory(DataSource dataSource) {
        var factory = new LocalContainerEntityManagerFactoryBean();
        factory.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        factory.setPackagesToScan("test.app");
        factory.setDataSource(dataSource);
        factory.setJpaProperties(additionalProperties());
        factory.afterPropertiesSet();
        return factory.getObject();
    }

    private Properties additionalProperties() {
        var props = new Properties();
        props.put("hibernate.dialect", env.getProperty("db.dialect", "org.hibernate.dialect.HSQLDialect"));
        props.put("hibernate.show_sql", "true");
        props.put("hibernate.hbm2ddl.auto", "validate");
        return props;
    }

    @Bean
    public TransactionManager txManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setDataSource(dataSource());
        jpaTransactionManager.setEntityManagerFactory(entityManagerFactory);
        return jpaTransactionManager;
    }
}
