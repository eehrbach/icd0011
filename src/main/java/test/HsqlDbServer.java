package test;

import org.hsqldb.server.Server;

import java.io.PrintWriter;

public class HsqlDbServer{

    public static void main(String[] args) {
        Server server = new Server();
        server.setDatabasePath(0, "mem:testdb;sql.syntax_pgs=true");
        server.setDatabaseName(0, "testdb");
        server.setLogWriter(new PrintWriter(System.out));
        server.start();
    }
}
