package test.util;

import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class FileUtil {

    public static String readFileFromClasspath(String pathOnClasspath) {
        try {
            var classpathResource = new ClassPathResource(pathOnClasspath);
            return StreamUtils.copyToString(classpathResource.getInputStream(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
