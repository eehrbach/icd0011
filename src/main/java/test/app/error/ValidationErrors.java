package test.app.error;

import java.util.ArrayList;
import java.util.List;

public class ValidationErrors {
    private List<ValidationError> errors;

    public List<ValidationError> getErrors() {
        return errors;
    }

    public void setErrors(List<ValidationError> errors) {
        this.errors = errors;
    }

    public void addError(ValidationError error) {
        if (getErrors() == null) {
            setErrors(new ArrayList<>());
        }
        getErrors().add(error);
    }
}
