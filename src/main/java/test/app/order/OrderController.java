package test.app.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import test.app.order.installment.Installment;
import test.app.order.installment.InstallmentService;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
public class OrderController {

    private OrderService orderService;
    private InstallmentService installmentService;

    @Autowired
    public OrderController(OrderService orderService,
                           InstallmentService installmentService) {
        this.orderService = orderService;
        this.installmentService = installmentService;
    }

    @GetMapping("orders")
    public List<Order> getOrders() {
        return orderService.getAll();
    }

    @GetMapping("orders/{orderId}")
    public Order getOrder(@PathVariable Long orderId) {
        return orderService.getOrder(orderId);
    }

    @GetMapping("orders/{orderId}/installments")
    public List<Installment> getInstallmentsForOrder(@PathVariable Long orderId,
                                                     @RequestParam("start")
                                                     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                             LocalDate installmentStartDate,
                                                     @RequestParam("end")
                                                     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                             LocalDate installmentEndDate) {
        return installmentService.generateInstallmentsFor(orderId, installmentStartDate, installmentEndDate);
    }

    @PostMapping(value = "orders", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Order newOrder(@RequestBody @Valid Order order) {
        return orderService.saveOrder(order);
    }

    @DeleteMapping("orders/{orderId}")
    public void deleteOrder(@PathVariable Long orderId) {
        orderService.deleteOrder(orderId);
    }

    @DeleteMapping("orders")
    public void deleteOrder() {
        orderService.deleteOrders();
    }
}
