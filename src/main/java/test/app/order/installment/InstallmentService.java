package test.app.order.installment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.app.order.OrderService;
import test.app.order.row.OrderRow;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class InstallmentService {
    private static final int MONTHLY_INSTALLMENT_DAY = 1;
    private static final BigDecimal MINIMUM_INSTALLMENT_AMOUNT = BigDecimal.valueOf(3);

    private OrderService orderService;

    @Autowired
    public InstallmentService(OrderService orderService) {
        this.orderService = orderService;
    }

    public List<Installment> generateInstallmentsFor(Long orderId,
                                                     LocalDate installmentStartDate,
                                                     LocalDate installmentEndDate) {
        var order = orderService.getOrder(orderId);
        BigDecimal sum = BigDecimal.ZERO;
        for (OrderRow row : order.getOrderRows()) {
            sum = sum.add(row.getPrice().multiply(BigDecimal.valueOf(row.getQuantity())));
        }
        return generateInstallments(sum, installmentStartDate, installmentEndDate);
    }

    /**
     * Disclaimer: Logic is similar to a last year's automatic testing course task:
     * https://bitbucket.org/eehrbach/i398/src/master/src/main/java/invoice/InvoiceRowGenerator.java
     */
    public List<Installment> generateInstallments(BigDecimal totalAmount, LocalDate startDate, LocalDate endDate) {
        if (totalAmount.compareTo(MINIMUM_INSTALLMENT_AMOUNT) <= 0) {
            return Collections.singletonList(new Installment(totalAmount, startDate));
        }

        LocalDate lastInstallmentDate = endDate;
        int installmentCount = getInstallmentCount(startDate, lastInstallmentDate);
        BigDecimal payablePerInstallment = totalAmount.divide(BigDecimal.valueOf(installmentCount), RoundingMode.FLOOR);
        if (payablePerInstallment.compareTo(MINIMUM_INSTALLMENT_AMOUNT) < 0) {
            int monthsToPay = totalAmount.divide(MINIMUM_INSTALLMENT_AMOUNT, RoundingMode.FLOOR).intValue();
            lastInstallmentDate = startDate.plusMonths(monthsToPay - 1);
            installmentCount = getInstallmentCount(startDate, lastInstallmentDate);
        }

        BigDecimal[] amounts = totalAmount.divideAndRemainder(BigDecimal.valueOf(installmentCount));
        List<Installment> installments = new ArrayList<>();
        int installmentsLeft = installmentCount;
        int installmentsWithRemainder = totalAmount.intValue() % installmentCount;
        for (LocalDate date = startDate; date.isBefore(lastInstallmentDate.plusDays(1L)); date = date.plusDays(1L)) {
            if (date.equals(startDate) || date.getDayOfMonth() == MONTHLY_INSTALLMENT_DAY) {
                installmentsLeft--;
                BigDecimal installmentAmount = amounts[0];

                if (installmentsWithRemainder > 0 && installmentsWithRemainder > installmentsLeft) {
                    installmentAmount = installmentAmount
                            .add(amounts[1].divide(BigDecimal.valueOf(installmentsWithRemainder), RoundingMode.UNNECESSARY));
                }

                installments.add(new Installment(installmentAmount, date));
            }
        }
        return installments;
    }

    private int getInstallmentCount(LocalDate startDate, LocalDate endDate) {
        return Long.valueOf(ChronoUnit.MONTHS.
                between(startDate.withDayOfMonth(MONTHLY_INSTALLMENT_DAY),
                        endDate.plusMonths(1))).intValue();
    }
}
