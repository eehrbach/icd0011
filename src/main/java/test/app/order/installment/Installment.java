package test.app.order.installment;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Installment {
    private BigDecimal amount;
    private LocalDate date;

    public Installment(BigDecimal amount, LocalDate date) {
        this.amount = amount;
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @JsonFormat(pattern="yyyy-MM-dd")
    public LocalDate getDate() {
        return date;
    }
}
