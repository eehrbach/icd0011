package test.app.order;

public class OrderDaoException extends RuntimeException {
    public OrderDaoException(String message) {
        super(message);
    }

    public OrderDaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
