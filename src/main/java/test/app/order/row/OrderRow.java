package test.app.order.row;

import org.hibernate.validator.constraints.Range;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Embeddable
public class OrderRow {
    @Column(name = "item_name")
    private String itemName;

    @NotNull(message= "Quantity may not be empty")
    @Range(min = 1)
    private Integer quantity;

    @NotNull(message= "Price may not be empty")
    @DecimalMin("0.001")
    private BigDecimal price;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
