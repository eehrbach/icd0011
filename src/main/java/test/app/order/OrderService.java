package test.app.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.app.dao.OrderDao;

import java.util.List;

@Service
public class OrderService {

    private OrderDao orderDao;

    @Autowired
    public OrderService(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Transactional
    public Order saveOrder(Order order) {
        return orderDao.save(order);
    }

    @Transactional
    public Order getOrder(Long id) {
        return orderDao.get(id);
    }

    @Transactional
    public List<Order> getAll() {
        return orderDao.getAll();
    }

    @Transactional
    public void deleteOrder(Long id) {
        orderDao.delete(id);
    }

    @Transactional
    public void deleteOrders() {
        orderDao.deleteAll();
    }
}
