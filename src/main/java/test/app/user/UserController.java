package test.app.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import test.app.dao.UserDao;

@RestController
public class UserController {

    private UserDao userDao;

    @Autowired
    public UserController(UserDao userDao) {
        this.userDao = userDao;
    }

    @GetMapping("users")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity allUsers() {
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("users/{userName}")
    @PreAuthorize("#userName == authentication.name or hasAuthority('ROLE_ADMIN')")
    public User getUserByName(@PathVariable String userName) {
        return userDao.getUserByUserName(userName);
    }
}
