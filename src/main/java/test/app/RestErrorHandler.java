package test.app;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import test.app.error.ValidationError;
import test.app.error.ValidationErrors;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log4j2
@RestControllerAdvice
public class RestErrorHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ValidationErrors handleValidationException(MethodArgumentNotValidException ex) {
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        ValidationErrors errors = new ValidationErrors();
        fieldErrors.forEach(fieldError -> {
            ValidationError error = new ValidationError();
            List<String> arguments = new ArrayList<>();
            error.setCode(fieldError.getField() + "." + fieldError.getCode());
            error.setArguments(arguments);
            if (fieldError.getArguments() != null) {
                Arrays.asList(fieldError
                        .getArguments())
                        .forEach(arg -> {
                            if (!(arg instanceof DefaultMessageSourceResolvable)) {
                                error.getArguments().add(String.valueOf(arg));
                            }
                        });
            }
            errors.addError(error);
        });
        return errors;
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(PersistenceException.class)
    public void handlePersistenceException(PersistenceException e) {
        log.warn("Persistence exception occured", e);
    }

}
