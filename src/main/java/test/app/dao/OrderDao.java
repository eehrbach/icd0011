package test.app.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import test.app.order.Order;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRED)
public class OrderDao {
    @PersistenceContext
    private EntityManager em;

    public List<Order> getAll() {
        TypedQuery<Order> query = em.createNamedQuery("Order.findAll", Order.class);
        return query.getResultList();
    }

    public Order get(Long id) {
        return em.find(Order.class, id);
    }

    public Order save(Order order) {
        em.persist(order);
        em.flush();
        return order;
    }

    public void delete(Long id) {
        em.remove(get(id));
    }

    public void deleteAll() {
        Query query = em.createQuery("DELETE FROM Order o");
        query.executeUpdate();
        em.flush();
    }
}
