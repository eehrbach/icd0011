package test.app.dao;

import org.springframework.stereotype.Repository;
import test.app.user.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Repository
public class UserDao {

    @PersistenceContext
    private EntityManager em;

    public User getUserByUserName(String userName) {
        TypedQuery<User> query = em.createQuery("SELECT u FROM User u WHERE userName = ?1", User.class);
        query.setParameter(1, userName);
        return query.getSingleResult();
    }
}
