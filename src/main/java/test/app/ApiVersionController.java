package test.app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiVersionController {

    @GetMapping("version")
    public String getVersion() {
        return "Version: 1";
    }
}
