package test.security;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import test.security.handlers.ApiAccessDeniedHandler;
import test.security.handlers.ApiAuthEntryPoint;
import test.security.handlers.ApiLogoutSuccessHandler;
import test.security.jwt.JwtAuthenticationFilter;
import test.security.jwt.JwtAuthorizationFilter;

import javax.sql.DataSource;

@Log4j2
@Configuration
@EnableWebSecurity()
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private static final String JWT_KEY = "r6m4GNX6voKiPh5pfCaWkQoG8d1E7aopsjdoaisjdoi12312356ioKiPh2pfCaWk59ioKiPh2h5pfCaWkQoG8d1E756io";

    private DataSource dataSource;

    @Autowired
    public SecurityConfig(DataSource dataSource) {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/api/login", "/api/version", "/api/logout").permitAll()
                .anyRequest()
                .fullyAuthenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable()
                .addFilterBefore(new JwtAuthorizationFilter(authenticationManager(), JWT_KEY), LogoutFilter.class)
                .addFilterAfter(new JwtAuthenticationFilter(authenticationManager(), "/api/login", JWT_KEY), LogoutFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint(new ApiAuthEntryPoint())
                .accessDeniedHandler(new ApiAccessDeniedHandler())
                .and()
                .logout().logoutUrl("/api/logout")
                .logoutSuccessHandler(new ApiLogoutSuccessHandler());
    }

    @Override
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {
        builder.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(new BCryptPasswordEncoder(10));
    }
}
