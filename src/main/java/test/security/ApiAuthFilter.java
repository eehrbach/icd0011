package test.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import test.security.handlers.ApiAuthFailureHandler;
import test.security.handlers.ApiAuthSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Log4j2
public class ApiAuthFilter extends AbstractAuthenticationProcessingFilter {
    public ApiAuthFilter(AuthenticationManager authenticationManager,
                         String url) {
        super(url);
        setAuthenticationManager(authenticationManager);
        setAuthenticationSuccessHandler(new ApiAuthSuccessHandler());
        setAuthenticationFailureHandler(new ApiAuthFailureHandler());
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        try {
            var objectMapper = new ObjectMapper();
            var loginCredentials = objectMapper.readValue(request.getInputStream(), LoginCredentials.class);
            var passwordAuthToken = new UsernamePasswordAuthenticationToken(
                    loginCredentials.getUserName(),
                    loginCredentials.getPassword());
            return getAuthenticationManager().authenticate(passwordAuthToken);
        } catch (Exception e) {
            throw new AuthenticationServiceException("Failed to initialize security context", e);
        }

    }
}
