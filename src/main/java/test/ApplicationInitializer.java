package test;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import test.config.DatasourceConfig;
import test.config.MvcConfig;
import test.security.SecurityConfig;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/api/*" };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { MvcConfig.class };
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] { SecurityConfig.class, DatasourceConfig.class };
    }
}
